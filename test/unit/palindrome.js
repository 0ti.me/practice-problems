const { d, expect, tquire } = deps;

const me = __filename;

const { isPalindrome } = tquire(me);

d(me, () => {
  it('should detect racecar as being a palindrome', () => {
    expect(isPalindrome('racecar')).to.equal(true);
  });

  it('should detect toot as being a palindrome', () => {
    expect(isPalindrome('toot')).to.equal(true);
  });

  it('should not detect toat as being a palindrome', () => {
    expect(isPalindrome('toat')).to.equal(false);
  });

  it('should not detect offfe as being a palindrome', () => {
    expect(isPalindrome('offfe')).to.equal(false);
  });
});
