const { d, expect, tquire } = deps;

const me = __filename;

const { parseCsv } = tquire(me);

d(me, () => {
  let csv;

  describe('given the example csv', () => {
    beforeEach(() => {
      csv = [
        'artist names,ticket price,date',
        'hank,$20,2023-04-18T00:00:00.000Z',
        'james,$25,2023-04-19T00:00:00.000Z',
        '"sonny bob, james",$1,2023-05-27T00:00:00.000Z',
      ].join('\n');
    });

    it('should return the expected value', () =>
      expect(parseCsv(csv)).to.deep.equal([
        {
          'artist names': 'hank',
          'ticket price': 2000,
          date: Date('2023-04-18T00:00:00.000Z'),
        },
        {
          'artist names': 'james',
          'ticket price': 2500,
          date: Date('2023-04-19T00:00:00.000Z'),
        },
        {
          'artist names': 'sonny bob, james',
          'ticket price': 100,
          date: Date('2023-05-27T00:00:00.000Z'),
        },
      ]));
  });
});
