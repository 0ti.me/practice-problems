const { d, expect, tquire } = deps;

const me = __filename;

const { reverseArrayInPlace } = tquire(me);

d(me, () => {
  describe('given the input [1,5,3,2]', () => {
    let input;

    beforeEach(() => {
      input = [1, 5, 3, 2];
      input.reverse = () => {
        throw new Error('cheater');
      };
    });

    describe('reverseArrayInPlace', () => {
      it('should mutate the initial array', () => {
        reverseArrayInPlace(input); // ignore the output

        /* the input should be mutated */
        expect(input).to.deep.equal([2, 3, 5, 1]);
      });

      it('should return [2,3,5,1]', () => {
        expect(reverseArrayInPlace(input)).to.deep.equal([2, 3, 5, 1]);
      });
    });
  });
});
