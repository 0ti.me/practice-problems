const { d, expect, tquire } = deps;

const me = __filename;

const { calculateTotalOfCart } = tquire(me);

d(me, () => {
  let coupons;
  let products;
  let taxRate;

  const expVal = (expected) =>
    expect(calculateTotalOfCart(products, coupons, taxRate)).to.equal(expected);

  beforeEach(() => {
    coupons = [];
    products = [];
    taxRate = 8.25;
  });

  it('should be zero given no coupons or products', () => expVal(0));

  describe('given one taxable: undefined product', () => {
    beforeEach(() => {
      products.push({
        price: 100,
        sku: '0',
      });
    });

    describe('and an applicable coupon with zero uses', () => {
      beforeEach(() => {
        coupons.push({
          discount: 50,
          id: '1',
          maxUses: 0,
          skus: ['0'],
        });
      });

      it('should evaluate the correct total', () => expVal(108));
    });

    describe('and an applicable coupon with one use', () => {
      beforeEach(() => {
        coupons.push({
          discount: 50,
          id: '1',
          maxUses: 1,
          skus: ['0'],
        });
      });

      it('should evaluate the correct total', () => expVal(54));
    });

    describe('and a non-applicable coupon with one use', () => {
      beforeEach(() => {
        coupons.push({
          discount: 50,
          id: '1',
          maxUses: 1,
          skus: ['1'],
        });
      });

      it('should evaluate the correct total', () => expVal(108));
    });

    it('should evaluate the correct total', () => expVal(108));
  });

  describe('given one taxable: true product', () => {
    beforeEach(() => {
      products.push({
        price: 100,
        sku: '0',
        taxable: true,
      });
    });

    it('should evaluate the correct total', () => expVal(108));
  });

  describe('given one taxable: false product', () => {
    beforeEach(() => {
      products.push({
        price: 100,
        sku: '0',
        taxable: false,
      });
    });

    it('should evaluate the correct total', () => expVal(100));
  });

  describe('given three products', () => {
    beforeEach(() => {
      products.push({
        price: 200,
        sku: '0',
        taxable: true,
      });

      products.push({
        price: 300,
        sku: '1',
        taxable: false,
      });

      products.push({
        price: 400,
        sku: '2',
      });
    });

    describe('and a two-use discount which applies to all three products, two taxable, one not', () => {
      beforeEach(() => {
        coupons.push({
          discount: 50,
          id: '3',
          maxUses: 2,
          skus: ['0', '1', '2'],
        });
      });

      it('should correctly apply the discount to the taxable products', () =>
        expVal(841));
    });

    it('should evaluate the correct total', () => expVal(950));
  });
});
