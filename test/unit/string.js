const { d, expect, tquire } = deps;

const me = __filename;

const { reverseString } = tquire(me);

d(me, () => {
  describe('given the input freddy', () => {
    let input;

    beforeEach(() => {
      input = 'freddy';
    });

    describe('reverseString', () => {
      it('should not mutate the initial string', () => {
        reverseString(input); // ignore the output

        /* it should still equal the same value */
        expect(input).to.equal('freddy');
      });

      it('should return ydderf', () => {
        expect(reverseString(input)).to.equal('ydderf');
      });
    });
  });
});
