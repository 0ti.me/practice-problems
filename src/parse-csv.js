/**
 * Given this csv file contents:
 * artist names,ticket price,date
 * hank,$20,2023-04-18T00:00:00.000Z
 * james,$25,2023-04-19T00:00:00.000Z
 * "sonny bob, james",$1,2023-05-27T00:00:00.000Z
 *
 * Produce this array of objects (date is an instance of the Date object):
 * [
 *   {
 *     'artist names': 'hank',
 *     'ticket price': 2000,
 *     date: Date('2023-04-18T00:00:00.000Z'),
 *   },
 *   {
 *     'artist names': 'james',
 *     'ticket price': 2500,
 *     date: Date('2023-04-19T00:00:00.000Z'),
 *   },
 *   {
 *     'artist names': 'sonny bob, james',
 *     'ticket price': 100,
 *     date: Date('2023-05-27T00:00:00.000Z'),
 *   },
 * ]
 */
function parseCsv(input) {
  throw new Error('implement me');
}

module.exports = {
  parseCsv,
}
