/**
 * Given an input string of characters, return a copy of that string that is reversed without
 * mutating the original string. Do this without using String.reverse().
 */
function reverseString(input) {
  /* add your code here */
  throw new Error('implement me');
}

module.exports = {
  reverseString,
};
