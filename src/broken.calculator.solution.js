/**
 * @param {number} startValue
 * @param {number} target
 * @return {number}
 */
const brokenCalc = (startValue, target) => {
  let depth = 0;
  let m1;
  let x2;
  let val;

  const root = { depth: 0 };
  let nodes = [root];
  let node;

  if (startValue === target) {
    return 0;
  }

  while ((node = nodes.shift())) {
    val = node.value === undefined ? startValue : node.value;

    m1 = val - 1;
    x2 = val * 2;

    if (x2 === target || m1 === target) {
      return node.depth + 1;
    } else if (val < target) {
      nodes.push({ depth: node.depth + 1, value: x2 });
      /*
    } else {
      console.error(
        `not adding ${x2} because ${val} is already larger than ${target}`,
      );
      */
    }

    nodes.push({ depth: node.depth + 1, value: m1 });
  }
};

const brokenCalc2 = (startValue, target) => {
  let ans = 0;

  while (target > startValue) {
    ans++;

    if (target % 2 == 1) {
      target++;
    } else {
      target /= 2;
    }
  }

  return ans + startValue - target;
};

if (require.main === module) {
  let a;
  let b;
  let ta;
  let tb;

  const aGTb = [];
  const bGTa = [];

  for (let i = 1; i < 99999; ++i) {
    for (let j = 1; j < 99999; ++j) {
      if (process.env.logging === 'false') {
        a = brokenCalc(i, j);
        b = brokenCalc2(i, j);
      } else {
        process.stdout.write(`[${i}, ${j}]: `);
        ta = Date.now();
        a = brokenCalc(i, j);
        ta = Date.now() - ta;
        process.stdout.write(a.toString());
        process.stdout.write(' ');
        tb = Date.now();
        b = brokenCalc2(i, j);
        tb = Date.now() - tb;
        process.stdout.write(b.toString());
        process.stdout.write(`ta=${ta}ms tb=${tb}ms\n`);
      }

      if (a > b) {
        aGTb.push({ i, j });
      } else if (b > a) {
        bGTa.push({ i, j });
      }
    }
  }

  console.error({ aGTb, bGTa });
}
