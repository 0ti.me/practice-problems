class Node {
  constructor(name) {
    this.name = name;
  }

  /*
  processNode() {
    console.error('other', this && this.name);
    // this refers to the instance of "this" Node just like Java

    function coolThing() {
      console.error('coolThing', this);
      // this refers coolThing
    }

    (() => {console.error('lambdaFunction', this && this.name)})();

    coolThing();
  }
  */

  get next() {
    return this.mNext;
  }

  set next(newNode) {
    this.mNext = newNode;
  }

  get prev() {
    return this.mPrev;
  }

  set prev(newNode) {
    this.mPrev = newNode;
  }

  deleteFromList() {
    // [Hank Brown] <=> [James Bond] <=> [Michael Fleck]
    if (this.prev) {
      this.prev.next = this.next;
    }

    if (this.next) {
      this.next.prev = this.prev;
    }
  }

  searchForName(name) {
    let ptr = this;

    while (ptr.name !== name || ptr === null) {
      ptr = ptr.next;
    }

    return ptr;
  }
}

let patientRoot = undefined;

const appendNode = (name) => {
  const newNode = new Node(name);

  if (patientRoot === undefined) {
    patientRoot = newNode;

    return;
  } else {
    let endNode = patientRoot;

    while (endNode.next) {
      endNode = endNode.next;
    }

    endNode.next = newNode;
    newNode.prev = endNode;
  }
}

appendNode('Hank Brown');
appendNode('James Bond');
appendNode('Michael Fleck');

const jamesBondNode = patientRoot.searchForName('James Bond');

console.error(jamesBondNode);

jamesBondNode.deleteFromList();

console.error(patientRoot);
console.error(patientRoot.next);

patientRoot.next.deleteFromList();

console.error(patientRoot);

/*
const processPatientRootNode = patientRoot.processNode.bind(patientRoot);

console.error('processing node without reassigning the function');

patientRoot.processNode();

console.error('processing node with reassigning the function');

processPatientRootNode();

*/
