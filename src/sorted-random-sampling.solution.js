/**
 * Imagine you have a huge dataset, billions of records for all you know, and
 * you want to select a random sampling from that dataset. You don't have a
 * list of the records, you only have two numbers: sampleSize and dataSetSize
 *
 * Given those pieces of information, write a function which returns a random
 * sampling of indices which could be used to select that size of a sample from
 * the enormous dataset.
 */

const nextInt = (maxExclusive, min = 0) =>
  Math.floor(Math.random() * (maxExclusive - min)) + min;

class SortedRandomSampling {
  constructor(count) {
    this.count = count;
    this.maxExc = count;
    this.tree = null;
  }

  addNext() {
    if (this.maxExc <= 0) {
      throw new Error(
        'have already added a full sampling, trying to get more entries when no more exist',
      );
    }

    let index = 0;
    let next = nextInt(this.maxExc);
    let nextSave = next;

    let ptr = this.tree;

    // short circuit out for the first element
    if (!ptr) {
      this.tree = { left: null, right: null, value: next };

      --this.maxExc;

      return;
    }

    // STEP 1: Count the number less or equal to (next + that number)
    this.inOrderAscending((ptr) => {
      if (next >= ptr.value) {
        ++next;
      } else {
        // short ciruit out because we're processing a popped node (meaning only right trees are remaining)
        // and the popped node was greater than next.
        return true;
      }
    });

    ptr = this.tree;

    const nextNode = { left: null, right: null, value: next };

    // STEP 2: Insert the new value into the tree
    while (ptr) {
      if (ptr.value > next) {
        if (ptr.left) {
          ptr = ptr.left;
        } else {
          ptr.left = nextNode;
          break;
        }
      } else {
        // ptr.value < next (ptr.value === next should never occur, indicates a bug)
        if (ptr.right) {
          ptr = ptr.right;
        } else {
          ptr.right = nextNode;
          break;
        }
      }
    }

    if (!ptr) {
      throw new Error('something went wrong');
    }

    // STEP 3: reduce the maximum available indices remaining
    --this.maxExc;
  }

  /**
   * @param cb: Function - returns truthy to abort, anything falsy to keep going
   */
  inOrderAscending(cb) {
    let ptr = this.tree;
    const ptrs = [];

    while (ptrs.length > 0 || ptr) {
      if (ptr) {
        ptrs.push(ptr);
        ptr = ptr.left;
      } else {
        ptr = ptrs.pop();

        if (!ptr || cb(ptr)) {
          // short circuit out because cb returned something truthy or we're at the last ptr
          break;
        }

        ptr = ptr.right;
      }
    }
  }

  print() {
    this.inOrderAscending((ptr) => {
      console.log(ptr.value);
    });
  }

  sample(size) {
    for (; size > 0; --size) {
      this.addNext();
    }

    return this;
  }
}

/**
 * @param quantity - the number of random 0-based indexes requested
 * @param count - the 1-based number of total entries
 * @returns Number[] - the list of random 0-based indexes selected
 */
const getRandomUniqueSamplingOfIndexes = (quantity, count) => {
  const s = new SortedRandomSampling(count);

  s.sample(quantity).print();
};

if (require.main === module) {
  getRandomUniqueSamplingOfIndexes(
    parseInt(process.argv[2], 10),
    parseInt(process.argv[3], 10),
  );
}
