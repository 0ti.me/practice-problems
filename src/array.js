/**
 * Given an input array, return the same instance of the array with reversed data
 * by mutating the original array. Do this without using Array.reverse().
 */
function reverseArrayInPlace(input) {
  /* add your code here */
  throw new Error('implement me');
}

module.exports = {
  reverseArrayInPlace,
};
