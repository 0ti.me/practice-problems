/**
 * Imagine you have a huge dataset, billions of records for all you know, and
 * you want to select a random sampling from that dataset. You don't have a
 * list of the records, you only have two numbers: sampleSize and dataSetSize
 *
 * Given those pieces of information, write a function which returns a random
 * sampling of indices which could be used to select that size of a sample from
 * the enormous dataset.
 */

/**
 * @param quantity - the number of random 0-based indexes requested
 * @param count - the 1-based number of total entries
 * @returns Number[] - the list of random 0-based indexes selected
 */
const getRandomUniqueSamplingOfIndexes = (quantity, count) => {
  // your code here
};
