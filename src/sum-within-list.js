/*
Sum Numbers
=-=-=-=-=-=-=-=
Write a program that, given a list of three numbers
determines if it is possible  to add two of the numbers
to arrive at the third.

Input:
Your program should read lines from standard input.
Each line contains a comma separated list of three integers

Output:
For each line from standard input, print 'true' or 'false'
to standard output if any of the two input numbers can be
added to arrive at the third, one per line.

Test 1

Test Input
1,2,3
Expected Output
true

Test 2

Test Input
3,1,2

Expected Output
true
*/

// implement here
const solution = (inp) => inp;

module.exports = solution;

if (require.main === module) {
  solution(
    ['1,2,3', '1,3,2', '2,1,3', '2,3,1', '3,1,2', '3,2,1', '3,4,5'].join('\n'),
  );
}
