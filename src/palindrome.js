/* A palindrome is a string that is identical when read forward or backward.
 *
 * Some examples of palindromes:
 * - 01210
 * - racecar
 */

/**
 * This function takes in a string and returns a boolean (true/false) indicating whether or not the
 * input string is a palindrome.
 */
function isPalindrome(input) {
  throw new Error('implement me');
}

module.exports = {
  isPalindrome,
};
