/**
 * Given some string consisting of characters including:
 * * (
 * * )
 * * [
 * * ]
 * * {
 * * }
 *
 * Find the least number of swaps to make the parentheses balanced.
 *
 * Example:
 * * ([{}]) is balanced
 * * ([{)]} is balanced
 * * ))(( is unbalanced
 * * (()) is balanced
 * * ()() is balanced
 *
 * Return that integer.
 */

const minSwaps = (str) => 0;

module.exports = minSwaps;
