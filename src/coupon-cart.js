/**
 * Type Product {
 *   price: number; // an integer representing the number of whole cents the product costs.
 *                  // (integer value 100 = $1.00)
 *   sku: string; // a string identifying the product (like a UPC code)
 *   taxable?: boolean; // taxable, if absent, is true, if present, use the truthy/falsy value there
 * }
 *
 * Type Coupon {
 *   discount: number; // an integer representing the number of whole cents which the coupon discounts the product.
 *                     // (integer value 100 = $1.00)
 *   id: string; // a string representing the unique ID of the coupon
 *   maxUses: number;  // the number of times for which a given coupon can be applied to a single cart
 *   skus: string[]; // an array of skus which are applicable for a given coupon
 * }
 */

/**
 * Given an array of products in a cart, an array of coupons, and a tax rate, calculate the total
 * cost of the products in the cart.
 *
 * Some products may have a property "taxable" which might be false. The tax rate should not
 * be used on products which are not taxable. Products without the property "taxable" are taxable as if
 * the "taxable" property were set to true.
 *
 * All products which are taxable should have taxes (rounded to the nearest number of cents)
 * added to the subtotal to calculate the total. Be sure to calculate tax from the discounted amount.
 *
 * All coupons for which an applicable product is in the cart should reduce the price up to the maxUses times,
 * prioritizing taxable products over non taxable products.
 *
 * Finally the total should be accumulated and returned.
 *
 * @param products - Product[] (array of type Product -- see above for type definition)
 * @param coupons - Coupon[] (array of type Coupon -- see above for type definition)
 * @param taxRate - double describing the percentage tax rate (double value 15.51 = 15.51%)
 *
 * @returns the total cost of the cart
 */
function calculateTotalOfCart(products, coupons, taxRate) {
  /* add your code here */
  throw new Error('implement me');
}

module.exports = {
  calculateTotalOfCart,
};
